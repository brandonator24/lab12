#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password will be


// Stucture to hold both a plaintext password and a hash.
typedef struct entry 
{
    // DONE: FILL THIS IN
    char plain[50];
    char hash[33];
} entry;

int qcomp(const void *a, const void *b)
    {
        entry *aa = (entry *)a;
        entry *bb = (entry *)b;
        
        return strcmp((*aa).hash, (*bb).hash);
    }

int hashSearch(const void *t, const void *elem)
{
    char *tt = (char *)t;
    entry *eelem = (entry *)elem;
    
    return strcmp(t, eelem->hash);
}

// TODO
// Read in the dictionary file and return an array of entry structs.
// Each entry should contain both the hash and the dictionary
// word.

struct entry *read_dictionary(char *filename, int *size)
{
    int len = 20;
    entry * arr = malloc(len * sizeof(entry));
    
    FILE *dictionary = fopen(filename, "r");
    if (!dictionary)
    {
        perror("Cannot open your dictionary file!");
        exit(1);
    }
    
    char line[50];
    int entries = 0;
    while (fgets(line, 50, dictionary) != NULL)
    {
        if (entries == len)
        {
            len += 20;
            arr = realloc(arr, len * sizeof(entry));
            printf("Added 20 spaces to array\n");
        }
        
        char *nl = strchr(line, '\n');
        if (nl != NULL)
        {
            *nl = '\0';
        }
        
        char *hash = md5(line, strlen(line));
        sscanf(hash, "%s", arr[entries].hash);
        
        sscanf(line, "%s", arr[entries].plain);
        //printf("plain: %s | hash: %s\n", arr[entries].plain, arr[entries].hash);
        entries++;
    }
    
    *size = entries;
    
    return arr;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // DONE: Read the dictionary file into an array of entry structures
    int entryLength;
    struct entry *dict = read_dictionary(argv[2], &entryLength);
    
    // DONE: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function that
    // sorts the array by hash value.
    qsort(dict, entryLength, sizeof(entry), qcomp);
    
    for (int i = 0; i < entryLength; i++)
        printf("%d: plain: %s | hash: %s\n", i, dict[i].plain, dict[i].hash);
    
    // Partial example of using bsearch, for testing. Delete this line
    // once you get the remainder of the program working.
    // This is the hash for "rockyou".
    //struct entry *found = bsearch("f806fc5a2a0d5ba2471600758452799c", dict, entryLength, sizeof(entry), hashSearch);
    //if (found)
        //printf("***FOUND***\n%s\n", *found);
        
    // TODO
    // Open the hash file for reading.
    
    FILE *hashes = fopen(argv[1], "r");
    if (!hashes)
    {
        perror("Cannot open your hash file!");
        exit(1);
    }
    
    char line[33];
    while (fgets(line, 33, hashes) != NULL)
    {
        char *nl = strchr(line, '\n');
        if (nl != NULL)
        {
            *nl = '\0';
        }
    
    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    struct entry *found = bsearch(line, dict, entryLength, sizeof(entry), hashSearch);

    // If you find it, get the corresponding plaintext dictionary word.
    // Print out both the hash and word.
    if(found)
    {
        foundCount++;
        printf("***FOUND***\nplain: %s | hash: %s\n", found, line);
    }
    
    // Need only one loop. (Yay!)
    
    }
}
